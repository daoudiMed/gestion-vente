
// l'Enumeration Promotion deffinissent les differents promotions possible.
	public enum Promotion {
		PromoETE ("ETE"), PromoPRINTEMPS("PRINTEMPS"), PromoAUTOMNE("AUTOMNE"), PromoHIVER("HIVER"), PromoNOMAL("");
		
		private String suffix;
		
		private Promotion (String suffix) {
			this.suffix = suffix;
		}
		
		public String getSuffix() {
			return suffix;
		}
	}


package packSocketsApp;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.*;
import java.util.Scanner;

public class Client {

	public static void main(String[] args){
		try {
			Scanner sc=new Scanner(System.in);
			
			//declaration du socket client
			Socket client=new Socket("127.0.0.1",10014);
			//declaration d'un flot de sortie
			BufferedWriter out= new BufferedWriter(new
			OutputStreamWriter(client.getOutputStream()));
			//proposition de saisi pr l'user
			System.out.println("Voulez vous demander un produit ?");
			String s=sc.nextLine();
			
			/*
			 * Nous pouvons faire une sorte de recherche cote clientsur le produit !! 
			*/
			
			//ecriture du flux de sortie
			out.write(s);
			//fermeture du flux et le socket
			out.close();
			
			client.close();
			} catch (UnknownHostException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
			} 
		} 
	}
package packSocketsApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

public class ServeurGestion {

	public static void main(String[] args){
		
		ServerSocket socket;
		
		try {
			socket = new ServerSocket(10014);
			Thread t = new Thread(new Accepter_clients(socket));
			t.start();
			System.out.println("Mes employeurs sont pr�ts !");
		
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
}

class Accepter_clients implements Runnable {

	   private ServerSocket serveur;
	   private Socket socket;
	   private int nbrclient = 1;
		public Accepter_clients(ServerSocket s){
			serveur = s;
		}
		
		public void run() {

	        try {
	        	while(true){
	        		Socket client = null;
	        		
	        		//System.out.println("ecout pour des connections :");
	        		try {
	        			client = serveur.accept();
	        			//System.out.println("ops2");
	        		}catch(IOException e) {
	        			System.out.println("[+] Erreur : Connection failed");
	        			System.exit(1);
	        		}
	        		
	        		System.out.println("[+] Connexion etablie avec un client");
	        		System.out.println("[+] Ecout pour des produit !!");
	        		
	        		PrintWriter output = new PrintWriter(client.getOutputStream(),true);
	        		BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
	        		
	        		String message = "";
	        		
	        		while((message = input.readLine())!=null) {
	        			System.out.println("Serveur : Le produit "+message+" est trouve");
	        			//output.println(message);
	        		}
	        		
	        		output.close();
	        		input.close();
	        		serveur.close();
	        		client.close();
	        	}
	        
	        } catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

package packSocketsApp;

import java.net.*;
import java.io.*;


public class EchoServeur {
	
	public static void main(String[] args) throws IOException {
		
		ServerSocket serveur =null;
		
		try {
			serveur = new ServerSocket(10002);
			
		}catch(IOException e) {
			System.out.println("[+] Erreur : le serveur peux pas ecouter sur le port 8000");
			System.exit(1);
		}
		
		Socket client = null;
		System.out.println("ecout pour des connections :");
		try {
			client = serveur.accept();
			System.out.println("ops2");
		}catch(IOException e) {
			System.out.println("[+] Erreur : Connection failed");
			System.exit(1);
		}
		
		System.out.println("[+] Connexion etablie");
		System.out.println("[+] Ecout pour des messages");
		
		PrintWriter output = new PrintWriter(client.getOutputStream(),true);
		BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
		
		String message = "";
		
		while((message = input.readLine())!=null) {
			System.out.println("Serveur : "+message);
			output.println(message);
		}
		
		output.close();
		input.close();
		serveur.close();
		client.close();
		
	}
	
}

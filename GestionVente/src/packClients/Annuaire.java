package packClients;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Annuaire {
	ArrayList<TelRecord> clients ;
	
	public Annuaire() {
		clients = new ArrayList<TelRecord>();
	}
	
	public void ajouterClient(TelRecord client) {
		clients.add(client);
	}
	
	public void retirerClient(TelRecord client) {
		clients.remove(client);
	}
	
	public void listerClients() {
		for (int i=0;i<clients.size();i++) {
			System.out.println(clients.get(i).getNom()+","+clients.get(i).getPrenom()+","+clients.get(i).getNumTel());
		}
	}
	
	// exporter tous les client de l'annuiaire 
	public void exporterClientsCSV(String fileName) {
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		    
			writer.write("Nom,Prenom,Numero");
			for (int i=0; i<clients.size(); i++) {
				writer.newLine();
				writer.write(clients.get(i).getNom()+","+clients.get(i).getPrenom()+","+clients.get(i).getNumTel());
				//System.out.println(articles.get(i).getTitre()+","+articles.get(i).getPrix()+","+articles.get(i).getQuantit�());
			}
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<TelRecord> importerCSV (String csvFilePath) {
		ArrayList<TelRecord> clients = new ArrayList<TelRecord>();
		
		File file = new File(csvFilePath);
		FileReader fr=null;
		try {
			// nous creons une connection avec le fichier de destination.
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			// nous lisons le fichier ligne par ligne.
			String line;
			
			//on saute la premiere ligne:
			br.readLine();
			
			// et on commence la lecture :
			while ((line = br.readLine()) !=null) {
				//System.out.println(line);
				
				String nom = line.split(",")[0];
				String prenom= line.split(",")[1];
				String numero = line.split(",")[2];
				
				clients.add(new TelRecord(nom,prenom,numero));
			}
			
			fr.close();
			br.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("erreur au niveau de la lecture du fichier");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("erreur au niveau de la lecture des ligne");
			e.printStackTrace();
		}
		this.clients = clients;
		
		return clients;
	}
	
	public static void main(String[] args) {
		
		TelRecord client1 = new TelRecord("med", "daoudi", "06352145");
		TelRecord client2 = new TelRecord("marwa", "daoudi", "058696663");
		
		// l'ajoue des information de qlq clients:
		Annuaire annuaire = new Annuaire();
		annuaire.ajouterClient(client1);
		annuaire.ajouterClient(client2);
		
		// affichage de l'annuaire:
		annuaire.listerClients();
		
		TelRecord client3 = new TelRecord("meryem", "ait hamou", "058696663");
		annuaire.ajouterClient(client3);
		
		//importation et exportation:
		//annuaire.exporterClientsCSV("./src/client.csv");
		//annuaire.importerCSV("./src/client.csv");
		
		
		// Nous creons deux threads un va nous enregistrer le client dans une liste et l autre nous permett de l'ajouter dans le fichier des clients
		TelRecord client4 = new TelRecord("lhousain", "ait hamou", "07899556");
		Thread t1 = new Thread(new AnnuaireThread(0,client4,"./src/client.csv"));
		Thread t2 = new Thread(new AnnuaireThread(1,client4,annuaire));
		t1.start();
		t2.start();
		
		System.out.println(annuaire);
		
		while (t2.isAlive()) {
			// cette boucle a pour objectif d'attendre le t2 jusqu'il sendorme ... !
		}
		
		annuaire.listerClients();
	}
	
}

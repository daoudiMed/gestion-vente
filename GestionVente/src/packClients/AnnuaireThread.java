package packClients;

import java.util.ArrayList;

import javax.swing.plaf.SliderUI;

public class AnnuaireThread implements Runnable{
	
	int tache;// la tache va nous specifier la tache a faire par le thread ! (si 0 on enregistre le client dans un fichier donnee si non o l'ajoute dans la liste donnee)
	TelRecord client;
	String link;
	public AnnuaireThread(int tache, TelRecord client, String link) {// la tache
		this.tache = tache;
		this.client = client;
		this.link = link;
	}
	
	Annuaire annuaire;
	public AnnuaireThread(int tache, TelRecord client, Annuaire annuaire) {
		this.tache = tache;
		this.client = client;
		this.annuaire = annuaire;
		
		System.out.println(annuaire);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		//synchronized(this) 
		{
			if (tache == 0) {
				client.exporterTelRecord(link);
			}
			else {
				annuaire.ajouterClient(client);
			}
	    }
	}
	
	
	
}
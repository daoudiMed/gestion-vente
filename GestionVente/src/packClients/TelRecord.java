package packClients;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TelRecord {
	private String nom, prenom, numTel;
	
	public TelRecord(String nom,String prenom,String numTel) {
		this.nom = nom;
		this.prenom = prenom;
		this.numTel = numTel;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}
	
	// exporter un seule client:
	public void exporterTelRecord(String csvFilePath) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath, true));
		    
				writer.write("\n"+getNom()+","+getPrenom()+","+getNumTel());
				//System.out.println(articles.get(i).getTitre()+","+articles.get(i).getPrix()+","+articles.get(i).getQuantit�());
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}


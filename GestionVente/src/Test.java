
public class Test {
	
	public static void main(String[] args) {
		
		// test de la class article:
		Article article = new Article("Pack PC", 15,1,Promotion.PromoAUTOMNE);
		
		// affichage des informations de l'article:
		System.out.println(article.toString()+"");
		
		// ajouter des produits a l'article :
		article.ajouterProduit("PC");
		article.ajouterProduit("Cartable");
		article.ajouterProduit("Un Cadeau+");
		
		// affichage des produit de l'article:
		article.afficherProduits();
		
	}
}

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Article {
	
	private String titre;
	private double prix;
	private int quantit�;
	private Promotion promo;
	
	// Liste des produits d'un article.
	private ArrayList<Produit> produits;
	
	public Article() {
		this.titre = null;
		this.prix = 0;
		this.quantit� = 0;
	}
	
	public Article(String titre, double prix, int quantit�, Promotion promo) {
		this.titre = titre;
		
		this.prix = prix;// initialisation du prix par le prix normale.
		this.promo = promo;
		
		this.prix = getPrix();
		
		this.quantit� = quantit�;
		this.produits = new ArrayList<Produit>();
	}

	public Article(String titre, double prix, int quantit�) {
		this.titre = titre;
		this.prix = prix;
		this.quantit� = quantit�;
		this.produits = new ArrayList<Produit>();
	}
	
	// les acceceurs accesseurs et mutateurs;

	public double getPrix() {
		
		// le prix ici est changer selon la promotion, ou retoune la meme valeur si il y a pas de promotion.
		switch(promo) {
			case PromoETE :
				prix = prix * 0.9;
				break;
			case PromoPRINTEMPS :
				prix = prix * 0.7;
				break;
			case PromoAUTOMNE :
				prix = prix * 0.95;
				break;
			case PromoHIVER :
				prix = prix * 0.8;
				break;
			default :
				prix = prix; // on peux meme eviter cette instruction.
				break;
		}
		return prix;
	}

	public String getTitre() {
		// le titre est changer selon la promotion.
		String titre ="";
		
		if (promo != null) {
			titre = this.titre+" "+promo.getSuffix();
		}
		
		return titre;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQuantit�() {
		return quantit�;
	}

	public void setQuantit�(int quantit�) {
		this.quantit� = quantit�;
	}
	
	@Override
	public String toString() {
		return "Article [titre=" + getTitre() + ", prix=" + getPrix() + ", quantit�=" + getQuantit�() + "]";
	}
	
	// l ajoue d'un produit a la liste des produit de l'article
	public void ajouterProduit(String titre) {
		produits.add(new Produit(titre));
	}
	
	// l'objectif de cette methode est d'ajouter plusieur produits 
	public void stockerProduits(ArrayList<String> produits) {
		for (int i=0;i<produits.size();i++)
			this.produits.add(new Produit(titre));
	}
	
	// l'affichage des differents produits de l'article
	public void afficherProduits() {
		System.out.println("\nles Produits de l'article: "+getTitre()+" sont:");
		for(int i=0;i<produits.size();i++) {
			System.out.println("* "+produits.get(i).getTitre());
		}
	}
	
	// la class interne d'un article, (l'article peux contenir plusieurs article et des cadeaux).
	public class Produit{
		private String titre;
		public Produit(String titre){
			this.titre = titre;
		}
		public String getTitre(){
			return titre;
		}
	}
	
	
	

}

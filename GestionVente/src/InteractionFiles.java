import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class InteractionFiles {
	
	public static void main(String[] args) {
		
		ResourceBundle res = ResourceBundle.getBundle("paramConf");
		
		String file_lecture = res.getString("file_lecture");
		String file_ecriture = res.getString("file_ecriture");
		System.out.println("les proprietes:");
		System.out.println(file_lecture);
		System.out.println(file_ecriture);
		//*************
		System.out.println();
		System.out.println("La lecture du contenue d'un fichier:");
		File file = new File("./src/fichier");
		FileReader fr=null;
		try {
			// nous creons une connection avec le fichier de destination.
			
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			// nous lisons le fichier ligne par ligne.
			String line;
			while ((line = br.readLine()) !=null) {
				System.out.println(line);
			}
			
			fr.close();
			br.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("erreur au niveau de la lecture du fichier");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("erreur au niveau de la lecture des ligne");
			e.printStackTrace();
		}
		//***********
		//l'ecriture dans un fichier 
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("./src/fichier",true));// true pour ajouter sur le contenue du fichier!
			writer.write("\nHello world!");
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//***********
		// le test de l'ajoue de plusieurs Article.
		ArrayList<Article> articles = new ArrayList<Article>();
		
		articles.add(new Article("Pack PC", 15,1,Promotion.PromoAUTOMNE));
		articles.add(new Article("Chaise", 10,2,Promotion.PromoHIVER));
		articles.add(new Article("Porte", 20,3,Promotion.PromoPRINTEMPS));
		
		//********
		// on test l'interaction avec les fichier CSV
		InteractionFiles intF = new InteractionFiles();
		// l'exportation des articles dans un fichier CSV 
		intF.exporterCSV(articles, "./src/Articles.csv");
		// L'importaion des articles du fichier CSV 
		ArrayList<Article> impArticles = intF.importerCSV("./src/Articles.csv");
		
		// l'affichage des article importer:
		System.out.println();
		System.out.println("les articles importer sont:");
		for (int i=0; i<articles.size(); i++) {
			System.out.println("Article "+i+": "+articles.get(i).getTitre()+","+articles.get(i).getPrix()+","+articles.get(i).getQuantit�());
		}
		
	}
	
	// l'exportation des articles dans un fichier CSV
	public void exporterCSV(ArrayList<Article> articles , String csvFilePath) {
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath));
		    
			writer.write("Titre,prix,quantit�");
			for (int i=0; i<articles.size(); i++) {
				writer.newLine();
				writer.write(articles.get(i).getTitre()+","+articles.get(i).getPrix()+","+articles.get(i).getQuantit�());
				//System.out.println(articles.get(i).getTitre()+","+articles.get(i).getPrix()+","+articles.get(i).getQuantit�());
			}
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	// L'IMPORTATION DES informations des articles d'un fichier CSV vers une liste.
	public ArrayList<Article> importerCSV (String csvFilePath) {
		
		ArrayList<Article> articles = new ArrayList<Article>();
		
		File file = new File(csvFilePath);
		FileReader fr=null;
		try {
			// nous creons une connection avec le fichier de destination.
			
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			// nous lisons le fichier ligne par ligne.
			String line;
			
			//on saute la premiere ligne:
			br.readLine();
			
			// et on commence la lecture :
			while ((line = br.readLine()) !=null) {
				//System.out.println(line);
				
				String titre = line.split(",")[0];
				double prix = Double.parseDouble(line.split(",")[1]);
				int quantite = Integer.parseInt(line.split(",")[2]);
				
				articles.add(new Article(titre,prix,quantite));
			}
			
			fr.close();
			br.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("erreur au niveau de la lecture du fichier");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("erreur au niveau de la lecture des ligne");
			e.printStackTrace();
		}
		
		return articles;
	}
	
	
	
}

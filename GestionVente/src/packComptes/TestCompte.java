package packComptes;

/*
 * Cette Classe a pour objectif de tester la class Compte:
 */
public class TestCompte implements Runnable{
	
	Compte compte ;
	String nom;
	int montant;

	public TestCompte(Compte compte, String nom, int montant) {
		this.compte = compte;
		this.montant = montant;
		this.nom = nom;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		synchronized(this) // Nous fesont une sorte de synchronization entre les threads.
		{
			while((compte.getSolde()) >0){ 
				System.out.println("solde en compte "+compte.getSolde()+" "+nom);
				compte.retirer(montant);
				System.out.println(nom+" effectue un retret le reste est "+compte.getSolde());
			}
	    } 
	}
	
	
	/*
	 * Nous voullons tester le comportement des deux threads! 
	 */
	public static void main(String[] args) {
		Compte compte = new Compte ();
		TestCompte rc1 = new TestCompte(compte,"t1",10);
		
	    Thread t1 = new Thread(rc1);
	    Thread t2 = new Thread(rc1);
		
		t1.start();
		t2.start();
	}
}
